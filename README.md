#Prueba

La siguiente es una prueba para evaluar conocimientos de HTML, CSS, JavaScript y diseño Responsive.

Se busca que cumpla con los requerimientos especificados dentro del tiempo límite para esta prueba que es de **6 horas**.

Se evaluará habilidad en HTML y CSS, además aspectos de organización de código, buenas prácticas, priorización y creatividad para resolver los requerimientos.

**Es necesario contar con una cuenta en Bitbucket para realizar este ejercicio.**

**Nota**:  En la carpeta [`assets/documents/`](assets/documents/) encontrarás un documento en el cuál se encuentran los textos que deberán estar presentes en la maqueta terminada,
se considerará como puntución extra que la sección donde aparece la imagen de la caja quedará como en la imagen [`assets/img/caja_extra.png`](assets/img/caja_extra.png)

##Ejercicio

1. Clonar este repositorio.

1. **Maquetación:** Deberás traducir el diseño ubicado en [`assets/design_psd/`](assets/design_psd/) a HTML+CSS.
En la carpeta [`assets/img/`](assets/img/) encontraras todas las imagenes necesarias para la maquetación las tipografías quedan a su elleción.
    * **Archivos PSD**
        * home.psd
   
1. **Requerimientos:** Traducir diseño a HTML+CSS. Deberás usar técnicas CSS3 y HTML5 cuando lo requiera. Además debe realizar el sitio responsive. **Se recomienda fuertemente** realizar al menos la maquetación para desktop y mobile. Utilizar FontAwesome donde sea necesario.


1. **SEO Friendly:** Comunidad 4uno busca siempre tener una buena posición en ránkings de búsqueda. Crear etiquetas necesarias para un buen **SEO** (hint: use las keywords: crédito,bienestar,seguridad).
¿Crees que se requieran cambios en la maqueta? ¿Cuáles?
**Opcional:** agregar share buttons y etiquetas para redes sociales (hint: [http://ogp.me](http://ogp.me)).

1. **Advanced CSS:** Puede usar frameworks a elección para escribir CSS (hint: materialize, bootstrap)teniendo en cuenta la compatibilidad con distintos browsers (hint opcional: Usar [BrowserStack](http://www.browserstack.com/) para chequear el renderizado en distintos navegadores).
Opcional: ¿cuál sería tu enfoque en la construcción del diseño?

1. **Para finalizar la tarea se requiere crear un archivo Zip con los archivos necesarios y enviar por correo a las direcciones incluidas en el correo antes de iniciar esta prueba.**